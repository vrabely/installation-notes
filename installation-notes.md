# Debian 12 bookworm

## Before installation


* Backup relevant direcories using `rsync -av --delete`
* Save Firefox bookmarks and logins
* Download image from https://www.debian.org/
* Copy it to [usb stick](https://www.debian.org/releases/stable/i386/ch04s03.en.html#usb-copy-isohybrid)


## Essentials setup

* Follow instructions and install base Debian

* Install [KDE](https://wiki.debian.org/KDE)
```
su
apt-get install task-kde-desktop
/sbin/reboot
```

* Install `sudo`
```
$ su
# apt update && apt upgrade
# apt install sudo
# sudo adduser $USER sudo
# sudo reboot
```

* Copy the backed-up files.


## Firefox

* Customize settings
* Install plugin Surfingkeys and uBlock Origin


## KDE customizations

* Set KRunner shortcut to Alt-Space
* Find Screen Edges in System Settings and add Desktop Grid, Show Desktop, Show Windows; shorten their activation delay
* Try out Application Dashboard instead of Application Launcher and customize Favorites
* Try out Panel on the right


## Home folder

* Make handy shortcuts
```
cd ~
touch -- -i  # against rm -rf
ln -s ~/Downloads/ ~/dls
ln -s ~/Desktop/ ~/dt
ln -s ~/Documents/ ~/docs
```

## Essential CLI

* Install basic tools
```
sudo apt-get install git wget curl neovim
```

* Generate new SSH keys
```
ssh-keygen -t ed25519 -C "<comment>"
```
* Clone https://gitlab.com/vrabely/installation-notes and update if necessary
* Copy git config 
```
cp dotfiles/.gitconfig ~/.gitconfig
```

* Switch to fish shell
```
sudo apt-get install fish
chsh -s /usr/bin/fish
sudo reboot
```
* Run `fish_config`. Use 'Informative Vcs' prompt and add handy abbreviations
* Create `~/.config/fish/config.fish` and add  abbreviations
```
abbr -a g git
abbr -a d docker
```
* Set editors
```
set -gx EDITOR nvim
set -gx VISUAL nvim
```
* Update fish path  with `set -U fish_user_paths ~/additional/paths`.

* Install yakuake
```
sudo apt-get install yakuake
```
* Set global yakuake shortcut to Ctrl+Space and use minimal theme

* Install fonts
```
apt-get install fonts-firacode fonts-cabin fonts-cantarell
```

## Konsole Themes

Copy theme and profiles
```
# Konsole
mkdir -p ~/.local/share/konsole/
cp themes/*.colorscheme ~/.local/share/konsole/
cp themes/*.profile ~/.local/share/konsole/

# Yakuake
mkdir -p ~/.local/share/yakuake/skins/
cp -r themes/yakuake-skin-breeze-minimal ~/.local/share/yakuake/skins/
```

Set `rc` files
```
cp dotfiles/konsolerc ~/.config/
cp dotfiles/yakuakerc ~/.config/
```


## apt-get

* Standard installation
```
apt-get install
# Tools
acpi
lm-sensors
sqlite3
tree

# Apps
djview4
inkscape
kdenlive
kolourpaint
krita
mupdf
vlc
# Games
# Python
python3-venv
python3-pip
```


## Rust

* Follow instructions on [Rust webpage](https://www.rust-lang.org/)

Install cli tools
```
cargo install \
    bat \
    bottom \
    broot \
    du-dust \
    exa \
    fd-find \
    just \
    hyperfine \
    procs \
    ripgrep
```

### Dev tools

Install
* [VS Code](https://code.visualstudio.com/docs/setup/linux) '¯\\_(ツ)_/¯'
* [Docker](https://docs.docker.com/engine/install/debian/) and [enable for non-root](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
* [Podman](https://podman.io/docs/installation)
* [kind](https://kind.sigs.k8s.io/) cluster
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management)
* [k9s](https://k9scli.io/) - just with webi
* [terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
* [qemu](https://wiki.debian.org/QEMU)
* [kvm](https://wiki.debian.org/KVM)


## Optional

### R
* Follow [the instructions](https://cran.r-project.org/bin/linux/debian/).
* Get the latest [RStudio](https://www.rstudio.com/products/rstudio/download/) and install it
```
sudo apt install -f ./rstudio-2021.09.1-372-amd64.deb
```
* Alternatively use [Rocker image](https://www.rocker-project.org/).

  * choose vim keybindgs, Light or Cobalt theme
  * check shortcuts: "insertPipeOperator" ("Alt+."), "switchFocusSourceConsole" ("Ctrl+J")


### Racket

* Follow instructions on [Racket webpage](https://racket-lang.org/).
* Install [pollen](https://docs.racket-lang.org/pollen/).


### Other
* Check [OpenWrt](https://openwrt.org/) for router
* Install wifi drivers `$ sudo apt install firmware-realtek firmware-iwlwifi`


### Nvidia graphic card (didn't work)
* Follow [Nvidia instructions](https://wiki.debian.org/NvidiaGraphicsDrivers)
[Install CUDA](https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Debian&target_version=10) and `nvtop` for convenience.




